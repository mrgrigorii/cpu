from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader

import pickle

with open('data/2_test_data.pickle', 'rb') as f:
    	data_new = pickle.load(f)

def index(request):
    example_text = 'Cinebench 11.5'
    data_header = ['Процессор', 'Примерная цена, руб', 
    			   'Производительность одного ядра', 'Производительность всех ядер', 
    			   'Цена за единицу производительности ядра, руб', 'Цена за единицу производительности всех ядер, руб']
    data = [[' '.join(i[:3]), round(i[3], -2), i[4], i[5], round(i[3]/i[4]), round(i[3]/i[5])] for i in data_new if i[4]!=0 and i[3]!=0]
    template = loader.get_template('index2.html')
    context = RequestContext(request, {
        'example_text': example_text,
        'data': data,
        'data_header': data_header,
    })
    return HttpResponse(template.render(context))
